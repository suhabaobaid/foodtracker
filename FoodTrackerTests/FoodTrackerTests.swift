//
//  FoodTrackerTests.swift
//  FoodTrackerTests
//
//  Created by Suha Baobaid on 12/08/2017.
//  Copyright © 2017 Suha Baobaid. All rights reserved.
//

import XCTest
@testable import FoodTracker

class FoodTrackerTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    // MARK: Meal class tests
    func testMealInitializationSucceeds() {

        // Zero rating
        let zeroMealRating = Meal.init(name: "test", photo: nil, rating: 0)
        XCTAssertNotNil(zeroMealRating)

        // Positive rating
        let positiveMealRating = Meal.init(name: "Test", photo: nil, rating: 4)
        XCTAssertNotNil(positiveMealRating)
    }
    func testMealInitializationFails() {

        // Negative rating
        let negativeMealRating = Meal.init(name: "test", photo: nil, rating: -1)
        XCTAssertNil(negativeMealRating)

        // Empty name
        let emptyMealName = Meal.init(name: "", photo: nil, rating: 2)
        XCTAssertNil(emptyMealName)

        //Rating exceeds the maximum
        let maxMealRating = Meal.init(name: "test", photo: nil, rating: 9)
        XCTAssertNil(maxMealRating)

    }

}
