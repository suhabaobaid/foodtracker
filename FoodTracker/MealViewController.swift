//
//  MealViewController.swift
//  FoodTracker
//
//  Created by Suha Baobaid on 12/08/2017.
//  Copyright © 2017 Suha Baobaid. All rights reserved.
//

import UIKit

class MealViewController: UIViewController, UITextFieldDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    // MARK: Properties
    @IBOutlet weak var mealNameField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Handle the text field's user input through delegate callbacks
        mealNameField.delegate = self

    }

    // MARK: UITextFieldDelegate

    // This is called after the textfield resigns being the first responder
    func textFieldDidEndEditing(_ textField: UITextField) {
    }

    // This is called when the user presses the return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hides the keyboard by resigning the state of the textfield being
        // the responder
        textField.resignFirstResponder()

        // Return value to indicate whether the return button should be processed
        return true
    }

    // MARK: UIImagePickerControllerDelegate

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled
        dismiss(animated: true, completion: nil)
    }

    // Function called when the user picks an image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        else {
            fatalError("Expected a dcitionary containing an image, but was provided the following \(info)")
        }

        //Set the photo that is picked
        imageView.image = selectedImage

        //Dismiss the imagepicker then
        dismiss(animated: true, completion: nil)
    }

    // MARK: Actions
    @IBAction func selectImageFromPhotoLibrary(_ sender: UITapGestureRecognizer) {

        // Hide the keybaoard
        mealNameField.resignFirstResponder()

        // UIImagePickerController
        let imagePickerController = UIImagePickerController()

        // Allow only to access the photoLibrary
        imagePickerController.sourceType = .photoLibrary

        // Makes sure that the ViewController is notified when a user picks an image
        imagePickerController.delegate = self

        present(imagePickerController, animated: true, completion: nil)

    }

}
